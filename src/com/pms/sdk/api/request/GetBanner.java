package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import com.pms.sdk.api.APIManager.APICallback;

import android.content.Context;

public class GetBanner extends BaseRequest {

	public GetBanner(Context context) {
		super(context);
	}

	public JSONObject getParam (String memberId) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("mid", memberId);
			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void request (String memberid, final APICallback apiCallback) {
		try {
			apiManager.call(API_GETBANNER_PMS, getParam(memberid), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			mPrefs.putString(PREF_BANNER_ID, json.getString("bannerId"));
			mPrefs.putString(PREF_BANNER_URL, json.getString("bannerUrl"));
			mPrefs.putString(PREF_BANNER_IMG, json.getString("bannerImg"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return true;
	}
}
