package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;

public class SetConfig extends BaseRequest {

	public SetConfig(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @param msgFlag
	 * @param notiFlag
	 * @return
	 */
	public JSONObject getParam (String msgFlag, String notiFlag, String campFlag) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("msgFlag", msgFlag);
			jobj.put("notiFlag", notiFlag);
			jobj.put("campFlag", campFlag);
			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (String msgFlag, String notiFlag, String campFlag, final APICallback apiCallback) {
		try {
			apiManager.call(API_SET_CONFIG, getParam(msgFlag, notiFlag, campFlag), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			mPrefs.putString(PREF_MSG_FLAG, json.getString("msgFlag"));
			mPrefs.putString(PREF_NOTI_FLAG, json.getString("notiFlag"));
			mPrefs.putString(PREF_CAMP_FLAG, json.getString("campFlag"));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
