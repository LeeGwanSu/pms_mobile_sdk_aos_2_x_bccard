package com.pms.sdk.api.request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;

public class ClickBanner extends BaseRequest {

	public ClickBanner(Context context) {
		super(context);
	}

	public JSONObject getParam (String memberId, JSONArray reads) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("mid", memberId);
			jobj.put("clicks", reads);
			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void request (String memberId, JSONArray reads, final APICallback apiCallback) {
		try {
			apiManager.call(API_CLICKBANNER_PMS, getParam(memberId, reads), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		return true;
	}
}
