package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;

public class UserCheck extends BaseRequest {

	public UserCheck(Context context) {
		super(context);
	}

	public JSONObject getParam () {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
			jobj.put("uuid", PMSUtil.getUUID(mContext));

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void request (final APICallback apiCallback) {
		try {
			apiManager.call(API_USERCHECK_PMS, getParam(), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean requiredResultProc (JSONObject json) {
		try {
			mPrefs.putBoolean(PREF_USER_CHECK_FLAG, json.getBoolean("result"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return true;
	}
}
