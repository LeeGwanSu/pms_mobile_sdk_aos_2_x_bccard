package com.pms.sdk.api.request;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.push.FCMRequestToken;
import com.pms.sdk.push.mqtt.MQTTService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

public class DeviceCert extends BaseRequest {

	public DeviceCert(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (JSONObject userData) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();

			// new version
			jobj.put("appKey", PMSUtil.getApplicationKey(mContext));
			String savedUuid = PMSUtil.getUUID(mContext);
			String uuidByAndroidId = PhoneState.generateUUID(mContext);
			if(TextUtils.isEmpty(savedUuid))
			{
				CLog.i("this user has not uuid, so it create uuid by androidId");
				savedUuid = uuidByAndroidId;
			}
			else
			{
				CLog.i("this user has uuid ("+savedUuid+")");
			}
			jobj.put("uuid", savedUuid);
			/**
			 * 2019.10.23 비씨카드 요청으로 DeviceCert API에 신규 UUID 발급방식(AndroidId) 파라미터를 추가함
			 */
			jobj.put("androidId", uuidByAndroidId);
			jobj.put("pushToken", PMSUtil.getGCMToken(mContext));
			jobj.put("custId", PMSUtil.getCustId(mContext));
			jobj.put("bcno", PMSUtil.getBcNumber(mContext));
			jobj.put("appVer", PhoneState.getAppVersion(mContext));
			jobj.put("os", "A");
			jobj.put("osVer", PhoneState.getOsVersion());
			jobj.put("device", PhoneState.getDeviceName());
			jobj.put("sessCnt", "1");

			if (userData != null) {
				jobj.put("userData", userData);
			}

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (final JSONObject userData, final APICallback apiCallback)
	{
		try
		{
			String custId = PMSUtil.getCustId(mContext);

			//2019.10.10 업체 요청으로 UUID 업체에서 직접 설정
//			boolean isNExistsUUID = PhoneState.createDeviceToken(mContext);
//			CLog.i("Device Cert request get UUID : " + (isNExistsUUID == true ? "Created" : "Exists"));

			if (!custId.equals(mPrefs.getString(PREF_LOGINED_CUST_ID)))
			{
				// 기존의 custId와 auth할려는 custId가 다르다면, DB초기화
				CLog.i("DeviceCert:new user");
				mDB.deleteAll();
			}

			String token = PMSUtil.getGCMToken(mContext);
			if (!TextUtils.isEmpty(token) && !NO_TOKEN.equals(token))
			{
				try
				{
					apiManager.call(API_DEVICE_CERT, getParam(userData), new APICallback()
					{
						@Override
						public void response (String code, JSONObject json)
						{
							if (CODE_SUCCESS.equals(code))
							{
								PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
								requiredResultProc(json);
							}

							if (apiCallback != null)
							{
								apiCallback.response(code, json);
							}
						}
					});
				}
				catch (Exception e)
				{
					CLog.e("(device cert, Is not empty Token: " + e);
				}
			}
			else
			{
				new FCMRequestToken(mContext, PMSUtil.getGCMProjectId(mContext), new FCMRequestToken.Callback()
				{
					@Override
					public void callback(boolean isSuccess, String message)
					{
						CLog.i("FCMRequestToken isSuccess? "+isSuccess+" / " + message);
						try
						{
							apiManager.call(API_DEVICE_CERT, getParam(userData), new APICallback()
							{
								@Override
								public void response (String code, JSONObject json)
								{
									if (CODE_SUCCESS.equals(code))
									{
										PMSUtil.setDeviceCertStatus(mContext, DEVICECERT_COMPLETE);
										requiredResultProc(json);
									}

									if (apiCallback != null)
									{
										apiCallback.response(code, json);
									}
								}
							});
						}
						catch (Exception e)
						{
							CLog.e("(device cert, Is not empty Token: " + e);
						}
					}
				}).execute();
			}
		}
		catch (Exception e)
		{
			CLog.e(e.getMessage());
		}
 	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	@SuppressLint("DefaultLocale")
	private boolean requiredResultProc (JSONObject json) {
		try {
			PMSUtil.setAppUserId(mContext, json.getString("appUserId"));
			PMSUtil.setEncKey(mContext, json.getString("encKey"));

			// set msg flag
			mPrefs.putString(PREF_MSG_FLAG, json.optString("msgFlag"));
			mPrefs.putString(PREF_NOTI_FLAG, json.optString("notiFlag"));
			mPrefs.putString(PREF_CAMP_FLAG, json.optString("campFlag"));
			mPrefs.putString(PREF_API_LOG_FLAG, json.optString("collectApiLogFlag"));
			mPrefs.putString(PREF_PRIVATE_LOG_FLAG, json.optString("collectPrivateLogFlag"));
			String custId = PMSUtil.getCustId(mContext);
			if (!StringUtil.isEmpty(custId)) {
				mPrefs.putString(PREF_LOGINED_CUST_ID, PMSUtil.getCustId(mContext));
			}

			/**
			 * 2019.10.23 서버에서 성공코드 전달받았을 경우 AndroidId 방식으로 UUID 업데이트함
			 */
			PMSUtil.setUUID(mContext, PhoneState.generateUUID(mContext));

			// set badge
//			Badge.getInstance(mContext).updateBadge(json.getString("newMsgCnt"));

			// readMsg
			final JSONArray readArray = PMSUtil.arrayFromPrefs(mContext, PREF_READ_LIST);
			if (readArray.length() > 0) {
				// call readMsg
				new ReadMsg(mContext).request(readArray, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							// delete readMsg
							new Prefs(mContext).putString(PREF_READ_LIST, "");
						}

						if (CODE_PARSING_JSON_ERROR.equals(code)) {
							for (int i = 0; i < readArray.length(); i++) {
								try {
									if ((readArray.get(i) instanceof JSONObject) == false) {
										readArray.put(i, PMSUtil.getReadParam(readArray.getString(i)));
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							new ReadMsg(mContext).request(readArray, new APICallback() {
								@Override
								public void response (String code, JSONObject json) {
									if (CODE_SUCCESS.equals(code)) {
										new Prefs(mContext).putString(PREF_READ_LIST, "");
									}
								}
							});
						}
					}
				});
			} else {
				CLog.i("readArray is null");
			}

			// clickMsg
			JSONArray clickArray = PMSUtil.arrayFromPrefs(mContext, PREF_CLICK_LIST);
			if (clickArray.length() > 0) {
				// call clickMsg
				new ClickMsg(mContext).request(clickArray, new APICallback() {
					@Override
					public void response (String code, JSONObject json) {
						if (CODE_SUCCESS.equals(code)) {
							// delete clickMsg
							new Prefs(mContext).putString(PREF_CLICK_LIST, "");
						}
					}
				});
			} else {
				CLog.i("clickArray is null");
			}

			// mqttFlag Y/N
			if (FLAG_Y.equals(PMSUtil.getMQTTFlag(mContext)))
			{
				String flag = json.getString("privateFlag");
				mPrefs.putString(PREF_PRIVATE_FLAG, flag);
				if (FLAG_Y.equals(flag))
				{
					String protocol = json.getString("privateProtocol");
					String protocolTemp = "";
					try {
						URI uri = new URI(PMSUtil.getMQTTServerUrl(mContext));
						if (protocol.equals(PROTOCOL_TCP)) {
							protocolTemp = protocol.toLowerCase() + "cp";
						} else if (protocol.equals(PROTOCOL_SSL)) {
							protocolTemp = protocol.toLowerCase() + "sl";
						}

						if (uri.getScheme().equals(protocolTemp)) {
							mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
						} else {
							mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
							mContext.stopService(new Intent(mContext, MQTTService.class));
						}
					} catch (NullPointerException e) {
						mPrefs.putString(PREF_PRIVATE_PROTOCOL, protocol);
					}
				}
				else
				{
					try
					{
						mContext.stopService(new Intent(mContext, MQTTService.class));
					}
					catch (Exception e) { }
				}
			}

			// LogFlag Y/N
			if ((FLAG_N.equals(mPrefs.getString(PREF_API_LOG_FLAG)) && FLAG_N.equals(mPrefs.getString(PREF_PRIVATE_LOG_FLAG))) == false) {
				setCollectLog();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void setCollectLog () {
		boolean isAfter = false;
		String beforeDate = mPrefs.getString(PREF_YESTERDAY);
		String today = DateUtil.getNowDateMo();

		try {
			isAfter = DateUtil.isDateAfter(beforeDate, today);
		} catch (Exception e) {
			isAfter = false;
		}

		if (isAfter) {
			mPrefs.putBoolean(PREF_ONEDAY_LOG, false);
		}

		if (mPrefs.getBoolean(PREF_ONEDAY_LOG) == false) {
			if (beforeDate.equals("")) {
				beforeDate = DateUtil.getNowDateMo();
				mPrefs.putString(PREF_YESTERDAY, beforeDate);
			}
			new CollectLog(mContext).request(beforeDate, null);
		}
	}
}
