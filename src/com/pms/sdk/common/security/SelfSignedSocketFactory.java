package com.pms.sdk.common.security;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * 
 * @author erzisk
 * @since 2013.06.04
 */
public class SelfSignedSocketFactory {

	/**
	 * get default socket factory
	 * 
	 * @param protocol
	 * @return
	 */
	public static SSLSocketFactory selfSignedSocketFactory (String protocol) {
		SSLContext sslContext = null;
		try {
			sslContext = SSLContext.getInstance(protocol);
			sslContext.init(null, new X509TrustManager[] { new X509TrustManager() {
				@Override
				public void checkClientTrusted (X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public void checkServerTrusted (X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public X509Certificate[] getAcceptedIssuers () {
					return new X509Certificate[0];
				}
			} }, new SecureRandom());
			return sslContext.getSocketFactory();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static SSLSocketFactory ApiSSLSocketFactory (String protocol) {
		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance(protocol);
			sc.init(null, null, null);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}

		return sc.getSocketFactory();
	}

}
