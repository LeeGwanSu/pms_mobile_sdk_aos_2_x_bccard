package com.pms.sdk;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.*;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.FCMRequestToken;
import com.pms.sdk.view.Badge;

import java.io.Serializable;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class)
 * @version [2013.10.07] pushMsg.java 생성 - push 데이터 key 변경<br>
 *          [2013.10.11] android-query -> volley 로 library 변경<br>
 *          [2013.10.14] push notification 출력 변경<br>
 *          [2013.10.16 15:09] logout시에 저장된 cust_id삭제<br>
 *          [2013.10.21 10:30] api call시에, appUserId 또는 encKey가 없을 시 에러 추가<br>
 *          [2013.10.21 11:22] api call시에, paging이 존재하는 api(newMsg.m)일 경우, 가장 마지막 호출에서만 apiCallback이 이뤄지게끔 수정<br>
 *          [2013.10.21 18:43] push 데이터 key 변경 (IPMSConsts)<br>
 *          [2013.10.25 14:15] deviceCert시 token이 없는경우 'noToken'으로 넘김<br>
 *          [2013.10.29 17:03] deviceCert시 token이 "noToken"일 경우도 loop<br>
 *          [2013.10.30 11:26] push 수신 시 notImg exception 처리 추가<br>
 *          [2013.11.01 11:15] CLog 새로운 버전으로 적용 및 소스에서 TAG 삭제!!!!<br>
 *          [2013.11.06 15:02] deviceCert parameter에 sessCnt추가<br>
 *          [2013.11.06 16:07] logined_cust_id 추가, deviceCert 및 loginPms시에 cust_id와 logined_cust_id를 비교하여 db delete 결정<br>
 *          [2013.11.15 10:38] APIManager에서 error발생 시에도 apiCalback수행<br>
 *          [2013.11.19 15:36] push popup finish thread 추가<br>
 *          [2013.11.26 09:22] click 관련 추가<br>
 *          [2013.11.26 16:42] MQTT Client 변경<br>
 *          [2013.11.26 21:13] deviceCert시 read 및 click api 호출<br>
 *          [2013.11.27 17:51] MQTT Client 사용시 로그 저장 루틴 추가 <br>
 *          [2013.12.04 17:47] push 수신 시 해당 msgId가 존재하면 noti하지 않게 변경<br>
 *          [2013.12.05 15:35] push popup 유지 시간 설정 추가 PMS.setPushPopupShowingTime(int)<br>
 *          [2013.12.10 18:21] MQTT KeepAlive Time mata-data에서 설정하도록 수정함.<br>
 *          [2013.12.11 16:07] PMSDB.deleteExpireMsg 로직 변경<br>
 *          [2013.12.12 18:34] Private Flag 추가<br>
 *          [2013.12.16 10:33] PushReceiver.onMessage에 synchronized추가<br>
 *          [2013.12.16 16:36] Push Popup 사용자가 설정할수 있도록 변경함. <br>
 *          [2013.12.17 13:19] MQTT Client 중복 실행 수정함. <br>
 *          [2014.01.13 09:31] MQTT Client 안정화 & Connect Check 부분 수정함. <br>
 *          [2014.02.07 17:12] Collect 부분 수정함. <br>
 *          [2014.02.11 10:18] CollectLog getParam 부분 수정함. <br>
 *          [2014.02.21 17:23] MQTT Client 안정화 및 Prvate 서버 Protocol 적용 가능하게 수정함.<br>
 *          [2014.02.25 10:12] 안쓰는 method 삭제함.<br>
 *          [2014.03.03 10:36] 팝업 셋팅 default 설정 가능하게 변경.<br>
 *          [2014.03.05 15:31] MQTT Client 관련 Service 추가.<br>
 *          [2014.03.05 20:37] MQTT Client STOP 재시작하는 현상 수정.<br>
 *          [2014.03.13 10:47] MQTT Client keepAlive 수정함. sleep mode 삭제.<br>
 *          [2014.03.28 09:29] DisusePms.m API 추가함.<br>
 *          [2014.04.15 16:53] Notification bar 이미지 noti에 대한 이벤트 적용.<br>
 *          [2014.04.28 16:54] getBanner API 추가 & NewMsg Flag 수정함.<br>
 *          [2014.04.29 14:55] Recovery Flag DB 업데이트 추가함. <br>
 *          [2014.05.09 10:47] getBanner 수정 & ClickBanner 추가함.<br>
 *          [2014.05.12 13:12] LogoutPms 호출시 DB 삭제 안함.<br>
 *          [2014.05.14 13:26] Popup 설정값 추가함.<br>
 *          [2014.05.15 19:30] GCM Token 값을 사용자가 직접적으로 넣을수 있게 변경함.<br>
 *          [2014.05.16 11:09] Google 쪽에 요청안하게 수정함. <br>
 *          [2014.05.16 14:08] GCM 쪽 값을 받을 것인지 안받을 것인지 설정하는 값 추가. <br>
 *          [2014.05.26 18:13] campFlag 추가함. <br>
 *          [2014.05.27 20:00] push message 값 추가함에 따른 루틴 수정. <br>
 *          [2014.06.05 18:34] ohpoint push message 값 추가함에 따른 루틴 수정. <br>
 *          [2014.06.25 09:56] 다른앱 사용시 팝업 미노출 & 자체 버그 수정함. <br>
 *          [2014.06.30 16:55] 팝업창 안뜨는 현상 버그 수정중. <br>
 *          [2014.07.15 16:49] UserCheck.m 추가함. <br>
 *          [2014.07.17 14:58] UserCheck.m 버그 사항 수정함. <br>
 *          [2014.10.21 14:23] Android OS 5.0에서 상태창 이미지 표시 안되는 버그 수정함.<br>
 *          [2015.03.13 16:27] 구글 정책에 따른 앱 실행중일떄만 팝업창 노출.<br>
 *          [2015.05.18 14:18] 팝업창 안뛰우도록 설정함.<br>
 *          [2015.06.15 14:08] DeviceCert & LoginPms시 BC Number값 설정하도록 수정함.<br>
 *          [2016.01.04 13:39] Notification Bar Big Text적용함.<br>
 *          [2016.04.29 00:46] X509TrustManager 코드 삭제하고 대응 코드 삽입함.<br>
 *          [2016.11.29 09:45] doze mode broadcast 패치1 적용 <br>
 *          [2016.12.12 18:00] SSL 관련 대응패치 RollBack, Noti아이콘 bug fix <br>
 *          [2017.10.24 15:23] * Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *          					* DB 중복 Open으로 인해 강제종료 문제 수정
 *          					* PushPopup 발생 시 강제 종료 문제 수정
 *          					* UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 *          					  UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함 <br>
 *         	[2017.11.23 10:28] * PhoneState 내부에서 UUID 발급을 IS_NEW_UUID로 분기 태움 NEW UUID일 경우 중복발송의
 *         							문제가 있을 수 있어 아직 사용하지 않도록 수정
 *         	[2018.03.21 09:27] * Android 8.0 적용 (JobScheduler, 명시적 호출, 버전에 따라 Notification Chnnel 적용)
 *         	[2018.10.08 09:01] MQTT Wakeup 적용
 *         	[2018.11.02 11:17] MQTT 수정
 *         	[2018.11.02 11:24] MQTT 라이브러리 업데이트
 *         	[2018.11.05 16:14] MQTT 수정
 *         	[2018.11.13 13:00] push receiver 수정
 *         	[2018.11.27 13:31] MQTT Starter 적용
 *         	[2018.11.28 16:33] startActivity 오류 대응
 *         	[2018.12.06 11:11] MQTT 최적화
 *         	[2018.12.07 16:05] MQTT 최적화
 *         	[2018.12.10 19:10] MQTT Binder 적용
 *         	[2018.12.11 09:43] 오류 수정
 *         	[2019.01.08 10:58] MQTT 버전별 분기 및 OOM 현상 대응
 *         	[2019.01.09 17:20] * MQTT KeepAliveTime 유동처리
 *         	                   * boot_complete 이후, private 체크로직 추가
 *			[2019.01.21 17:20] * GCM - getInstance 8.0 이상 에러 대응(jobIntent Service)
 *		                       * MQTT 서비스 유지시간
 *          [2019.01.31 11:35] * startMQTTService(), stopoMQTTService() 제거
 *                             * PopupActivity 사용 시, settingPopupUI()의 내용 중 NullPointException 보완 코드 추가
 *			[2019.02.18 09:55] * FCM 전환
 *							   * 기기인증 시(deviceCert), token 값이 없을 경우 처리 추가
 *			[2019.04.12 14:14] Token 발급 로직 변경 (deviceCert 비동기 처리 >> TMS 초기화 시점, FCM Service 등 추가 보안책 추가)
 *			[2019.04.12 17:19] 알림소리 원복기능 제거
 *			[2019.04.16 16:06] 초기화 토큰 요청 로직 변경
 *			[2019.04.17 15:35] 토큰 발급 개선
 *			[2019.05.20 09:33] Private Service 안정화 로직 적용
 *			[2019.07.19 18:17] 버그수정
 *			[2019.10.10 13:52] 채널 재생성 로직 제거, Badge 관련된 동작 제거, UUID 로직 변경(ANDROID_ID), TLS 대응, ReadMsg kk->HH 대응, 업체 요청으로 UUID 직접 입력하도록 수정
 *			[2019.10.10 17:33] Android X 대응
 *			[2019.10.23 16:26] 업체 요청을 DeviceCert API에 androidId 방식으로 생성한 UUID 값을 전달하는 파라미터 추가함
 *			[2019.11.04 17:22] iOS가 pushToken을 uuid로 쓰고 있어 null로 전달되는 현상이 발생해서 기존 저장한 UUID 없을 경우 androidId 값 전달하도록 수정
 *			[2019.12.10 17:08] 업체 요청으로 푸시 알림 그룹화 적재화 기능 추가
 */
public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;
	private static PMS instancePms;
	private static PMSPopup instancePmsPopup;
	private Context mContext;

	private final PMSDB mDB;
	private final Prefs mPrefs;

	private OnReceivePushListener onReceivePushListener;

	/**
	 * constructor
	 * 
	 * @param context
	 */
	private PMS(final Context context) {
		this.mDB = PMSDB.getInstance(context);
		this.mPrefs = new Prefs(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:201912101708");

		initOption(context);

		/**
		 * refresh db thread
		 */
	}

	/**
	 * initialize option
	 */
	private void initOption (Context context) {
		if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
			mPrefs.putString(PREF_NOTI_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
			mPrefs.putString(PREF_MSG_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
			mPrefs.putString(PREF_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
			mPrefs.putString(PREF_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
			mPrefs.putString(PREF_ALERT_FLAG, "Y");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
			mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
			mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
		}
		if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
			PMSUtil.setServerUrl(context, API_SERVER_URL);
		}
		if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
			mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
		}
		if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
			mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
		}
	}

	/**
	 * getInstance
	 *
	 * @param context
	 * @return
	 */
	public static PMS getInstance (Context context)
	{
		return getInstance(context, "");
	}

	/**
	 * getInstance
	 * 
	 * @param context
	 * @param projectId
	 * @return
	 */
	public static PMS getInstance (final Context context, String projectId)
	{
		if (instancePms == null)
		{
			instancePms = new PMS(context);
		}

		instancePms.setmContext(context);
		if (!TextUtils.isEmpty(projectId))
		{
			PMSUtil.setGCMProjectId(context, projectId);
			CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		}

		String token = PMSUtil.getGCMToken(context);
		if(NO_TOKEN.equals(token) || TextUtils.isEmpty(token))
		{
			new FCMRequestToken(context, PMSUtil.getGCMProjectId(context), new FCMRequestToken.Callback() {
				@Override
				public void callback(boolean isSuccess, String message)
				{
					CLog.i("FcmRequestToken isSuccess? "+isSuccess+ " message "+message);
				}
			}).execute();
		}

		return instancePms;
	}

	public static PMSPopup getPopUpInstance () {
		return instancePmsPopup;
	}

	/**
	 * clear
	 * 
	 * @return
	 */
	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
			instancePms.unregisterReceiver();
			instancePms = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

	private void unregisterReceiver () {
		try
		{
			Badge.getInstance(mContext).unregisterReceiver();
		}
		catch (Exception e)
		{
			CLog.i("unregisterReceiver() receiver is not registered");
		}
	}

	// /**
	// * push token 세팅
	// * @param pushToken
	// */
	public void setPushToken (String pushToken) {
		PMSUtil.setGCMToken(mContext, pushToken);
	}

	public void setSenderID (String senderid) {
		PMSUtil.setGCMProjectId(mContext, senderid);
	}

	public void isGCMPush (Boolean isgcm) {
		PMSUtil.setIsGCMPush(mContext, isgcm);
	}

	public void setUseBigText (Boolean state) {
		mPrefs.putString(PREF_USE_BIGTEXT, state ? "Y" : "N");
	}

	public void setPopupSetting (Boolean state, String title) {
		instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
	}

	private void setUUID(String uuid)
	{
		PMSUtil.setUUID(mContext, uuid);
	}
	public String getUUID()
	{
		return PMSUtil.getUUID(mContext);
	}

	/**
	 * set ServerUrl
	 * 
	 * @param serverUrl
	 */
	public void setServerUrl (String serverUrl) {
		CLog.i("setServerUrl");
		CLog.d("setServerUrl:serverUrl=" + serverUrl);
		PMSUtil.setServerUrl(mContext, serverUrl);
	}

	/**
	 * cust id 세팅
	 * 
	 * @param custId
	 */
	public void setCustId (String custId) {
		CLog.i("setCustId");
		CLog.d("setCustId:custId=" + custId);
		PMSUtil.setCustId(mContext, custId);
	}

	public void setBcNumber (String bcno) {
		PMSUtil.setBcNumber(mContext, bcno);
	}

	public String getBcNumber () {
		return PMSUtil.getBcNumber(mContext);
	}

	/**
	 * get cust id
	 * 
	 * @return
	 */
	public String getCustId () {
		CLog.i("getCustId");
		return PMSUtil.getCustId(mContext);
	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	/**
	 * set inbox activity
	 * 
	 * @param inboxActivity
	 */
	public void setInboxActivity (String inboxActivity) {
		mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
	}

	/**
	 * set push popup activity
	 * 
	 * @param classPath
	 */
	public void setPushPopupActivity (String classPath) {
		mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
	}

	public void setOnMessageReceiver (String classPath) {
		mPrefs.putString(PREF_PUSH_RECEIVER_CLASS, classPath);
	}
	
	/**
	 * set pushPopup showing time
	 * 
	 * @param pushPopupShowingTime
	 */
	public void setPushPopupShowingTime (int pushPopupShowingTime) {
		mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
	}

	/**
	 * get msg flag
	 * 
	 * @return
	 */
	public String getMsgFlag () {
		return mPrefs.getString(PREF_MSG_FLAG);
	}

	/**
	 * get noti flag
	 * 
	 * @return
	 */
	public String getNotiFlag () {
		return mPrefs.getString(PREF_NOTI_FLAG);
	}

	/**
	 * get camp flag
	 * 
	 * @return
	 */
	public String getCampFlag () {
		return mPrefs.getString(PREF_CAMP_FLAG);
	}

	/**
	 * set noti icon
	 * 
	 * @param resId
	 *//*
	public void setNotiIcon (int resId) {
		mPrefs.putInt(PREF_NOTI_ICON, resId);
	}

	*//**
	 * set large noti icon
	 * 
	 * @param resId
	 *//*
	public void setLargeNotiIcon (int resId) {
		mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
	}*/

	/**
	 * set noti cound
	 * 
	 * @param resId
	 */
	public void setNotiSound (int resId) {
		mPrefs.putInt(PREF_NOTI_SOUND, resId);
	}

	/**
	 * set noti receiver
	 * 
	 * @param intentAction
	 */
	public void setNotiReceiver (String intentAction) {
		mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
	}

	/**
	 * set noti receiver class
	 *
	 * @param intentClass
	 */
	public void setNotiReceiverClass(String intentClass) {
		mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, intentClass);
	}
	public void setPushReceiverClass(String intentClass) {
		mPrefs.putString(IPMSConsts.PREF_PUSH_RECEIVER_CLASS, intentClass);
	}
	
	/**
	 * set ring mode
	 * 
	 * @param isRingMode
	 */
	public void setRingMode (boolean isRingMode) {
		mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
	}

	/**
	 * set vibe mode
	 * 
	 * @param isVibeMode
	 */
	public void setVibeMode (boolean isVibeMode) {
		mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	/**
	 * set popup noti
	 * 
	 * @param isShowPopup
	 */
	public void setPopupNoti (boolean isShowPopup) {
		mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	/**
	 * set screen wakeup
	 * 
	 * @param isScreenWakeup
	 */
	public void setScreenWakeup (boolean isScreenWakeup) {
		mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
	}

	public OnReceivePushListener getOnReceivePushListener () {
		return onReceivePushListener;
	}

	public void setOnReceivePushListener (OnReceivePushListener onReceivePushListener) {
		this.onReceivePushListener = onReceivePushListener;
	}

	/**
	 * set debugTag
	 * 
	 * @param tagName
	 */
	public void setDebugTAG (String tagName) {
		CLog.setTagName(tagName);
	}

	/**
	 * set debug mode
	 * 
	 * @param debugMode
	 */
	public void setDebugMode (boolean debugMode) {
		CLog.setDebugMode(debugMode);
	}

	public String getBannerID () {
		return mPrefs.getString(PREF_BANNER_ID);
	}

	public String getBannerUrl () {
		return mPrefs.getString(PREF_BANNER_URL);
	}

	public String getBannerImg () {
		return mPrefs.getString(PREF_BANNER_IMG);
	}

	public Boolean geteUserCheck () {
		return mPrefs.getBoolean(PREF_USER_CHECK_FLAG);
	}

	@Deprecated
	public void bindBadge (Context c, int id) {
//		Badge.getInstance(c).addBadge(c, id);
	}

	@Deprecated
	public void bindBadge (TextView badge) {
//		Badge.getInstance(badge.getContext()).addBadge(badge);
	}

	/**
	 * show Message Box
	 * 
	 * @param c
	 */
	public void showMsgBox (Context c) {
		showMsgBox(c, null);
	}

	public void showMsgBox (Context c, Bundle extras) {
		CLog.i("showMsgBox");
		if (PhoneState.isAvailablePush()) {
			try {
				Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

				Intent i = new Intent(mContext, inboxActivity);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				if (extras != null) {
					i.putExtras(extras);
				}
				c.startActivity(i);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			// Intent i = new Intent(INBOX_ACTIVITY);
			// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// if (extras != null) {
			// i.putExtras(extras);
			// }
			// c.startActivity(i);
		}
	}

	@Deprecated
	public void closeMsgBox (Context c) {
//		Intent i = new Intent(c, Badge.class).setAction(RECEIVER_CLOSE_INBOX);
//		c.sendBroadcast(i);
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgID
	 * @return
	 */
	public Msg selectMsgWhereUserMsgId (String userMsgID) {
		return mDB.selectMsgWhereUserMsgId(userMsgID);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteUserMsgId (String userMsgId) {
		return mDB.deleteUserMsgId(userMsgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param MsgId
	 * @return
	 */
	public long deleteMsgId (String MsgId) {
		return mDB.deleteMsgId(MsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */
	public void setNotificationGroupable(boolean enable)
	{
		DataKeyUtil.setDBKey(mContext, DB_NOTIFICATION_GROUPABLE, enable?"Y":"N");
	}
	public void setNotificationStackable(boolean enable)
	{
		DataKeyUtil.setDBKey(mContext, DB_NOTIFICATION_STACKABLE, enable?"Y":"N");
	}
}