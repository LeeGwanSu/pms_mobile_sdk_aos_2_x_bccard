package com.pms.sdk.push;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.api.QueueManager;
import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.mqtt.MQTTBinder;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.BitmapLruCache;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.List;

import androidx.core.app.NotificationCompat;

/**
 * push receiver
 *
 * @author erzisk
 * @since 2013.06.07
 */
public class PushReceiver extends BroadcastReceiver implements IPMSConsts {

    // notification id
    public final static int NOTIFICATION_ID = 0x253470;
    private static final int NOTIFICATION_GROUP_SUMMARY_ID = 1;
    private static final String NOTIFICATION_GROUP = "com.pms.sdk.notification_type";
    private static int sNotificationId = NOTIFICATION_GROUP_SUMMARY_ID + 1;
    private static final int START_TASK_TO_FRONT = 2;
    private static final int DEFAULT_SHOWING_TIME = 30000;
    private final Handler mFinishHandler = new Handler();
    private Prefs mPrefs;
    private PowerManager pm;
    private PowerManager.WakeLock wl;

    /**
     * finish runnable
     */
    private final Runnable finishRunnable = new Runnable() {
        @Override
        public void run() {
            if (wl != null && wl.isHeld()) {
                wl.release();
            }
        }
    };
    private Bitmap mPushImage;

    @Override
    public synchronized void onReceive(final Context context, final Intent intent)
    {

        if (intent == null || intent.getAction() == null)
        {
            if (intent != null)
                CLog.e("intent action is null, intent : " + intent.toString());
            return;
        }

        CLog.i("onReceive() -> " + intent.toString());
        mPrefs = new Prefs(context);

        String message = intent.getStringExtra(MQTTBinder.KEY_MSG);

        // set push info
        try {
            JSONObject msgObj = new JSONObject(message);
            if (msgObj.has(KEY_MSG_ID)) {
                intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
            }
            if (msgObj.has(KEY_NOTI_TITLE)) {
                intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
            }
            if (msgObj.has(KEY_MSG_TYPE)) {
                intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
            }
            if (msgObj.has(KEY_NOTI_MSG)) {
                intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
            }
            if (msgObj.has(KEY_MSG)) {
                intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
            }
            if (msgObj.has(KEY_SOUND)) {
                intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
            }
            if (msgObj.has(KEY_NOTI_IMG)) {
                intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
            }
            if (msgObj.has(KEY_DATA)) {
                intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // receive push message
        if (MQTTBinder.ACTION_RECEIVED_MSG.equals(intent.getAction()))
        {
            // private server
            CLog.i("onReceive:receive from private server");

        }
        else if (intent.getAction().equals(ACTION_RECEIVE))
        {
            // gcm
            CLog.i("onReceive:receive from FCM(GCM)");
        }

        if (isImagePush(intent.getExtras())) {
            // image push
            QueueManager queueManager = QueueManager.getInstance();
            RequestQueue queue = queueManager.getRequestQueue();
            queue.getCache().clear();
            ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());

            imageLoader.get(intent.getStringExtra(KEY_NOTI_IMG), new ImageListener() {
                @Override
                public void onResponse(ImageContainer response, boolean isImmediate) {
                    if (response == null) {
                        CLog.e("response is null");
                        return;
                    }
                    if (response.getBitmap() == null) {
                        CLog.e("bitmap is null");
                        return;
                    }
                    mPushImage = response.getBitmap();
                    CLog.i("imageWidth:" + mPushImage.getWidth());
                    onMessage(context, intent);
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    CLog.e("onErrorResponse:" + error.getMessage());
                    // wrong img url (or exception)
                    onMessage(context, intent);
                }
            });

        } else {
            // default push
            onMessage(context, intent);
        }

//        if (intent.getAction().equals(ACTION_REGISTRATION)) {
//            // registration
//            CLog.i("onReceive:registration");
//            if (intent.getStringExtra(KEY_GCM_TOKEN) != null) {
//                // regist gcm key
//                CLog.d("handleRegistration:key=" + intent.getStringExtra(KEY_GCM_TOKEN));
//                mPrefs.putString(KEY_GCM_TOKEN, intent.getStringExtra(KEY_GCM_TOKEN));
//            } else {
//                // error occurred
//                CLog.i("handleRegistration:error=" + intent.getStringExtra("error"));
//                CLog.i("handleRegistration:unregistered=" + intent.getStringExtra("unregistered"));
//            }
//
//            return;
//        } else {
//            // receive push message
//            if (intent.getAction().equals(MQTTBinder.ACTION_RECEIVED_MSG)) {
//                // private server
//                String message = intent.getStringExtra(MQTTBinder.KEY_MSG);
//
//                CLog.i("onReceive:receive from private server");
//
//                // set push info
//                try {
//                    JSONObject msgObj = new JSONObject(message);
//                    if (msgObj.has(KEY_MSG_ID)) {
//                        intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
//                    }
//                    if (msgObj.has(KEY_NOTI_TITLE)) {
//                        intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
//                    }
//                    if (msgObj.has(KEY_MSG_TYPE)) {
//                        intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
//                    }
//                    if (msgObj.has(KEY_NOTI_MSG)) {
//                        intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
//                    }
//                    if (msgObj.has(KEY_MSG)) {
//                        intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
//                    }
//                    if (msgObj.has(KEY_SOUND)) {
//                        intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
//                    }
//                    if (msgObj.has(KEY_NOTI_IMG)) {
//                        intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
//                    }
//                    if (msgObj.has(KEY_DATA)) {
//                        intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } else if (intent.getAction().equals(ACTION_RECEIVE)) {
//                // gcm
//                // key: title, msgType, message, sound, data
//                CLog.i("onReceive:receive from GCM");
//            }
//
//            if (isImagePush(intent.getExtras())) {
//                // image push
//                QueueManager queueManager = QueueManager.getInstance();
//                RequestQueue queue = queueManager.getRequestQueue();
//                queue.getCache().clear();
//                ImageLoader imageLoader = new ImageLoader(queue, new BitmapLruCache());
//
//                imageLoader.get(intent.getStringExtra(KEY_NOTI_IMG), new ImageListener() {
//                    @Override
//                    public void onResponse(ImageContainer response, boolean isImmediate) {
//                        if (response == null) {
//                            CLog.e("response is null");
//                            return;
//                        }
//                        if (response.getBitmap() == null) {
//                            CLog.e("bitmap is null");
//                            return;
//                        }
//                        mPushImage = response.getBitmap();
//                        CLog.i("imageWidth:" + mPushImage.getWidth());
//                        onMessage(context, intent);
//                    }
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        CLog.e("onErrorResponse:" + error.getMessage());
//                        // wrong img url (or exception)
//                        onMessage(context, intent);
//                    }
//                });
//
//            } else {
//                // default push
//                onMessage(context, intent);
//            }
//        }
    }

    /**
     * on message (gcm, private msg receiver)
     *
     * @param context
     * @param intent
     */
    @SuppressLint("DefaultLocale")
    private synchronized void onMessage(final Context context, Intent intent) {
        CLog.d("onMessage!!!!!!!");
        
        final Bundle extras = intent.getExtras();

        PMS pms = PMS.getInstance(context);

        PushMsg pushMsg = new PushMsg(extras);
        CLog.i(pushMsg + "");

        if (TextUtils.isEmpty(pushMsg.msgId) || "".equals(pushMsg.msgId))
        {
            CLog.i("msgId or notiTitle or notiMsg or msgType is null");

            String privateFlag = PMSUtil.getPrivateFlag(context);
            String mktFlag = PMSUtil.getMQTTFlag(context);
            CLog.d("privateFlag: " + privateFlag + ", mktFlag: " + mktFlag);

            if(FLAG_Y.equals(privateFlag) && FLAG_Y.equals(mktFlag))
            {
                Intent i = new Intent(context, RestartReceiver.class);
                i.setAction(MQTTService.ACTION_FORCE_START);
                context.sendBroadcast(i);
            }

            return;
        }

        PMSDB db = PMSDB.getInstance(context);

        // check already exist msg
        Msg existMsg = db.selectMsgWhereMsgId(pushMsg.msgId);
        if (existMsg != null && existMsg.msgId.equals(pushMsg.msgId)) {
            CLog.i("already exist msg");
            return;
        }

        // insert (temp) new msg
        Msg newMsg = new Msg();
        newMsg.readYn = Msg.READ_N;
        newMsg.msgGrpCd = "999999";
        newMsg.expireDate = "0";
        newMsg.msgId = pushMsg.msgId;

        db.insertMsg(newMsg);

        // refresh list and badge
//        context.sendBroadcast(new Intent(RECEIVER_PUSH).putExtras(extras));
        // 명시적 Intent 호출로 변경
        Intent intentPush = null;

        String receiverClass = null;
//        receiverClass = ProPertiesFileUtil.getString(context, PRO_RECEIVER_CLASS);
        receiverClass = mPrefs.getString(IPMSConsts.PREF_PUSH_RECEIVER_CLASS);
        if (receiverClass != null) {
            try {
                Class<?> cls = Class.forName(receiverClass);
                intentPush = new Intent(context, cls).putExtras(extras);
                intentPush.setAction(RECEIVER_PUSH);
            } catch (ClassNotFoundException e) {
                CLog.e(e.getMessage());
            }
        }
        if (intentPush == null) {
            intentPush = new Intent(RECEIVER_PUSH).putExtras(extras);
        }

        if (intentPush != null) {
            context.sendBroadcast(intentPush);
        }
        // show noti
        // ** 수정 ** 기존 코드는 PREF_MSG_FLAG로 되어 있어 NOTI_FLAG로 변경 함
        CLog.i("NOTI FLAG : " + mPrefs.getString(PREF_NOTI_FLAG));

        if (FLAG_Y.equals(mPrefs.getString(PREF_NOTI_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG)))
        {
            // check push flag
            if (FLAG_Y.equals(mPrefs.getString(IPMSConsts.PREF_SCREEN_WAKEUP_FLAG)) || !StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG)))
            {
                // screen on
                pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "myapp:mywakelocktag");
                if (!pm.isScreenOn())
                {
                    wl.acquire();
                    mFinishHandler.postDelayed(finishRunnable, DEFAULT_SHOWING_TIME);
                }
            }

            CLog.i("version code :" + Build.VERSION.SDK_INT);

            // execute push noti listener
            if (pms.getOnReceivePushListener() != null) {
                if (pms.getOnReceivePushListener().onReceive(context, extras)) {
                    showNotification(context, extras);
                    CLog.i("ReceivePush onReceive !!!");
                } else {
                    CLog.i("ReceivePush !!!");
                    showNotification(context, extras);
                }
            } else {
                CLog.i("ReceivePush is null !!!");
                showNotification(context, extras);
            }

            CLog.i("ALERT FLAG : " + mPrefs.getString(PREF_ALERT_FLAG));

            CLog.i("POPUP NO FLAG : " + pushMsg.data);
            JSONObject date = null;
            String value = "";
            try {
                date = new JSONObject(pushMsg.data);
                value = date.getString("l");
            } catch (JSONException e) {
                CLog.e("json parsing error. " + e.getMessage());
            }

            if (FLAG_Y.equals(mPrefs.getString(PREF_ALERT_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
                if ((value.equals(KEY_BCCARD_FLAG) == false) && (value.equals(KEY_OHPOINT_FLAG1) == false)
                        && (value.equals(KEY_OHPOINT_FLAG2) == false)) {
                     showPopup(context, extras);
                }
            }
        }
    }

    /**
     * show notification
     *
     * @param context
     * @param extras
     */
    private synchronized void showNotification(Context context, Bundle extras) {

        // push intent sample
        // String msgId = extras.getString(KEY_MSG_ID);
        // String title = extras.getString(KEY_TITLE);
        // String msg = extras.getString(KEY_MSG);
        // String msgType = extras.getString(KEY_MSG_TYPE);
        // String sound = extras.getString(KEY_SOUND);
        // String data = extras.getString(KEY_DATA);
        CLog.i("showNotification");
        if (isImagePush(extras)) {
            showNotificationImageStyle(context, extras);
        } else {
            showNotificationTextStyle(context, extras);
        }
    }

    /**
     * show notification text style
     *
     * @param context
     * @param extras
     */
    @SuppressLint("NewApi")
    private synchronized void showNotificationTextStyle(final Context context, Bundle extras) {
        CLog.i("showNotificationTextStyle");
        // push info
        PushMsg pushMsg = new PushMsg(extras);

        // notification
        int iconId = PMSUtil.getIconId(context);
        int largeIconId = PMSUtil.getLargeIconId(context);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;

        String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
        if (StringUtil.isEmpty(strNotiChannel)) {
            strNotiChannel = "0";
        }

        // Notification channel added
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1 && PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1)
        {
            strNotiChannel = createNotiChannel(context, notificationManager, strNotiChannel);
            builder = new NotificationCompat.Builder(context, strNotiChannel);
            builder.setNumber(0);
        }
        else
        {
            builder = new NotificationCompat.Builder(context);
            builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
        }
        int notificationId = getNewNotificationId();
        builder.setContentIntent(makePendingIntent(context, extras, notificationId));
        builder.setAutoCancel(true);
        builder.setContentText(pushMsg.notiMsg);
        builder.setContentTitle(pushMsg.notiTitle);
        builder.setTicker(pushMsg.notiMsg);
        // setting lights color
        builder.setLights(Notification.FLAG_SHOW_LIGHTS, 1000, 2000);

        // set small icon
        CLog.i("small icon :" + iconId);
        if (iconId > 0) {
            builder.setSmallIcon(iconId);
        }

        // set large icon
        CLog.i("large icon :" + largeIconId);
        if (largeIconId > 0) {
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
        }

        // setting ring mode
        int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();
        // checek ring mode
        if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
            if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
                try
                {
                    int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
                    CLog.d("notiSound : "+notiSound);
                    if (notiSound > 0) {
                        Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
                        builder.setSound(uri);
                    } else {
                        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        builder.setSound(uri);
                    }
                } catch (Exception e) {
                    CLog.e(e.getMessage());
                }
            }
        }

        // check vibe mode
        if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
            builder.setDefaults(Notification.DEFAULT_VIBRATE);
        }

        if (FLAG_Y.equals(mPrefs.getString(PREF_USE_BIGTEXT))) {
            builder.setStyle(new NotificationCompat.BigTextStyle()
                    .setBigContentTitle(pushMsg.notiTitle).bigText(pushMsg.notiMsg));
        }

        if(!DataKeyUtil.getDBKey(context, DB_NOTIFICATION_STACKABLE).equals("Y"))
        {
            notificationManager.cancelAll();
        }
        if(DataKeyUtil.getDBKey(context, DB_NOTIFICATION_GROUPABLE).equals("Y"))
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                builder.setGroup(NOTIFICATION_GROUP);
                updateNotificationSummary(context, notificationManager);
            }
        }
        notificationManager.notify(notificationId, builder.build());
    }

    /**
     * show notification image style
     *
     * @param context
     * @param extras
     */
    @SuppressLint("NewApi")
    private synchronized void showNotificationImageStyle(final Context context, Bundle extras) {
        // push info
        PushMsg pushMsg = new PushMsg(extras);

        // notification
        int iconId = PMSUtil.getIconId(context);
        int largeIconId = PMSUtil.getLargeIconId(context);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder builder;

        if (PMSUtil.getTargetVersion(context) > Build.VERSION_CODES.N_MR1 &&
                Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            NotificationChannel notiChannel = new NotificationChannel(
                    NOTIFICATION_ID + "", PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(notiChannel);
            builder = new Notification.Builder(context, NOTIFICATION_ID + "");
        } else {
            builder = new Notification.Builder(context);
            builder.setNumber(PMSDB.getInstance(context).selectNewMsgCnt());
        }

        int notificationId = getNewNotificationId();
        builder.setContentIntent(makePendingIntent(context, extras, notificationId));
        builder.setAutoCancel(true);
        builder.setContentText(PMSUtil.getBigNotiContextMsg(context));
        builder.setContentTitle(pushMsg.notiTitle);
        builder.setTicker(pushMsg.notiMsg);
        // setting lights color
        builder.setLights(0xFF00FF00, 1000, 2000);

        // set small icon
        CLog.i("small icon :" + iconId);
        if (iconId > 0) {
            builder.setSmallIcon(iconId);
        }

        // set large icon
        CLog.i("large icon :" + largeIconId);
        if (largeIconId > 0) {
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
        }

        // setting ring mode
        int noti_mode = ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();
        // checek ring mode
        if (noti_mode == AudioManager.RINGER_MODE_NORMAL) {
            if (FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
                try {
                    int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
                    CLog.d("notiSound : "+notiSound);
                    if (notiSound > 0) {
                        Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
                        builder.setSound(uri);
                    } else {
                        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        builder.setSound(uri);
                    }
                } catch (Exception e) {
                    CLog.e(e.getMessage());
                }
            }
        }

        // check vibe mode
        if (FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)) || StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
            builder.setDefaults(Notification.DEFAULT_VIBRATE);
        }

        if (mPushImage == null) {
            CLog.e("mPushImage is null");
        }

        // show notification
        Notification notification = new Notification.BigPictureStyle(builder).bigPicture(mPushImage).setBigContentTitle(pushMsg.notiTitle)
                .setSummaryText(pushMsg.notiMsg).build();
        if(!DataKeyUtil.getDBKey(context, DB_NOTIFICATION_STACKABLE).equals("Y"))
        {
            notificationManager.cancelAll();
        }
        if(DataKeyUtil.getDBKey(context, DB_NOTIFICATION_GROUPABLE).equals("Y"))
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            {
                builder.setGroup(NOTIFICATION_GROUP);
                updateNotificationSummary(context, notificationManager);
            }
        }
        notificationManager.notify(notificationId, notification);
    }

    /**
     * make pending intent
     *
     * @param context
     * @param extras
     * @return
     */
    private PendingIntent makePendingIntent(Context context, Bundle extras, int requestCode) {
        // notification
        Intent innerIntent = null;
//      String receiverClass = ProPertiesFileUtil.getString(context, PRO_NOTI_RECEIVER);
        String receiverClass = mPrefs.getString(PREF_NOTI_RECEIVER_CLASS);
        String receiverAction = mPrefs.getString(PREF_NOTI_RECEIVER);
        CLog.i("makePendingIntent receiverClass : " + receiverClass);
        CLog.i("makePendingIntent receiverAction : " + receiverAction);
        if (receiverClass != null) {
            try {
                Class<?> cls = Class.forName(receiverClass);
                innerIntent = new Intent(context, cls).putExtras(extras);
                if (receiverAction != null)
                    innerIntent.setAction(receiverAction);
            } catch (ClassNotFoundException e) {
                CLog.e(e.getMessage());
            }
        }

        if (innerIntent == null) {
            CLog.d("innerIntent == null");
            receiverAction = receiverAction != null ? receiverAction : "com.pms.sdk.notification";
            // setting push info to intent
            innerIntent = new Intent(receiverAction).putExtras(extras);
        }

        if (innerIntent == null) return null;
        return PendingIntent.getBroadcast(context, requestCode, innerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    /**
     * show popup (activity)
     *
     * @param context
     * @param extras
     */
    @SuppressLint("DefaultLocale")
    private synchronized void showPopup(Context context, Bundle extras) {
        try {
            Class<?> pushPopupActivity;

            String pushPopupActivityName = mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY);

            if (StringUtil.isEmpty(pushPopupActivityName)) {
                pushPopupActivityName = DEFAULT_PUSH_POPUP_ACTIVITY;
            }

            try {
                pushPopupActivity = Class.forName(pushPopupActivityName);
            } catch (ClassNotFoundException e) {
                CLog.e(e.getMessage());
                pushPopupActivity = PushPopupActivity.class;
            }
            CLog.i("pushPopupActivity :" + pushPopupActivityName);

            Intent pushIntent = new Intent(context, pushPopupActivity);
            pushIntent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
            pushIntent.putExtras(extras);

            if (isOtherApp(context)) {
                context.startActivity(pushIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings({"deprecation", "static-access"})
    @SuppressLint("DefaultLocale")
    private boolean isOtherApp(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        String topActivity = "";

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            RunningAppProcessInfo currentInfo = null;
            Field field = null;
            try {
                field = RunningAppProcessInfo.class.getDeclaredField("processState");
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }

            List<RunningAppProcessInfo> appList = am.getRunningAppProcesses();
            for (RunningAppProcessInfo app : appList) {
                if (app.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    Integer state = null;
                    try {
                        state = field.getInt(app);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                    if (state != null && state == START_TASK_TO_FRONT) {
                        currentInfo = app;
                        break;
                    }
                }
            }

            if (currentInfo == null)
            {
                return false;
            }

            topActivity = currentInfo.processName;
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(10);
            topActivity = taskInfo.get(0).topActivity.getPackageName();
        }

        CLog.e("TOP Activity : " + topActivity);
        if (topActivity.equals(context.getPackageName())) {
            return true;
        }
        return false;
    }

    /**
     * is image push
     *
     * @param extras
     * @return
     */
    private boolean isImagePush(Bundle extras) {
        try {
            if (!PhoneState.isNotificationNewStyle()) {
                //throw new Exception("wrong os version");
                return false;
            }
            String notiImg = extras.getString(KEY_NOTI_IMG);
            CLog.i("notiImg:" + notiImg);
            if (notiImg == null || "".equals(notiImg)) {
                //throw new Exception("no image type");
                return false;
            }
            return true;
        } catch (Exception e) {
            CLog.e("isImagePush:" + e.getMessage());
            return false;
        }
    }

    private boolean isPmsPush(Bundle extras) {
        try {
            String pushType = extras.getString(KEY_MSG_TYPE);
            if (pushType == null || "".equals(pushType)) {
                //throw new Exception("no PMS Push!");
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * show instant view (toast)
     *
     */
    // private void showInstantView(Context context, String msg) {
    // LayoutInflater mInflater = LayoutInflater.from(context);
    // View v = mInflater.inflate(R.layout.pms_push_instant_view, null);
    // LinearLayout layPushMsg = (LinearLayout) v.findViewById(R.id.pms_lay_push_msg);
    // TextView txtPushTitle = (TextView) v.findViewById(R.id.pms_txt_push_title);
    // TextView txtPushMsg = (TextView) v.findViewById(R.id.pms_txt_push_msg);
    //
    // txtPushMsg.setText(msg);
    //
    // String currentTheme = Prefs.getString(context, Consts.PREF_THEME);
    // if (currentTheme.equals(context.getString(R.string.pms_txt_green))) {
    // layPushMsg.setBackgroundColor(0xCCC1C95E);
    // txtPushTitle.setTextColor(0xFFC1DF7D);
    // } else if (currentTheme.equals(context.getString(R.string.pms_txt_pink))) {
    // layPushMsg.setBackgroundColor(0xCCF695CA);
    // txtPushTitle.setTextColor(0xFFFED6F0);
    // } else if (currentTheme.equals(context.getString(R.string.pms_txt_brown))) {
    // layPushMsg.setBackgroundColor(0xCCD09F76);
    // txtPushTitle.setTextColor(0xFFFDD9B9);
    // } else if (currentTheme.equals(context.getString(R.string.pms_txt_black))) {
    // layPushMsg.setBackgroundColor(0xCC5C5C5C);
    // txtPushTitle.setTextColor(0xFF949494);
    // } else {
    // layPushMsg.setBackgroundColor(0xCCC1C95E);
    // txtPushTitle.setTextColor(0xFFC1DF7D);
    // }
    //
    // txtPushMsg.setTextColor(0xFFFFFFFF);
    //
    // Toast toast = new Toast(context);
    // toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
    // toast.setDuration(Toast.LENGTH_SHORT);
    // toast.setView(v);
    // toast.show();
    // }
    public int getNewNotificationId () {
        int notificationId = sNotificationId++;

        // Unlikely in the sample, but the int will overflow if used enough so we skip the summary
        // ID. Most apps will prefer a more deterministic way of identifying an ID such as hashing
        // the content of the notification.
        if (notificationId == NOTIFICATION_GROUP_SUMMARY_ID) {
            notificationId = sNotificationId++;
        }
        return notificationId;
    }

    public static int getNotificationId () {
        return sNotificationId;
    }

    @TargetApi(Build.VERSION_CODES.O)
    private String createNotiChannel(Context context, NotificationManager notificationManager, String strNotiChannel)
    {
        NotificationChannel notiChannel = notificationManager.getNotificationChannel(strNotiChannel);
        boolean isShowBadge = false;
        boolean isPlaySound;
        boolean isPlaySoundChanged = false;
        boolean isEnableVibe;
        boolean isShowBadgeOnChannel;
        boolean isPlaySoundOnChannel;
        boolean isEnableVibeOnChannel;

        try
        {
            if(IPMSConsts.FLAG_Y.equals((String) context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)))
            {
                isShowBadge = true;
            }
            else
            {
                isShowBadge = false;
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            CLog.e(e.getMessage());
        }

        if(IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_RING_FLAG)))
        {
            isPlaySound = true;
        }
        else
        {
            isPlaySound = false;
        }
        if(IPMSConsts.FLAG_Y.equals(mPrefs.getString(PREF_VIBE_FLAG)))
        {
            isEnableVibe = true;
        }
        else
        {
            isEnableVibe = false;
        }

        try
        {
            CLog.d("AppSetting isShowBadge "+ context.getPackageManager().getApplicationInfo(context.getPackageName(),
                    PackageManager.GET_META_DATA).metaData.get(IPMSConsts.META_DATA_NOTI_O_BADGE)+
                    " isPlaySound "+mPrefs.getString(PREF_RING_FLAG)+
                    " isEnableVibe "+mPrefs.getString(PREF_VIBE_FLAG));
        }
        catch (PackageManager.NameNotFoundException e)
        {
            CLog.e(e.getMessage());
        }

        if (notiChannel == null)
        {    //if notichannel is not initialized
            CLog.d("notificationChannel is initialized");
            notiChannel = new NotificationChannel(strNotiChannel, PMSUtil.getApplicationName(context), NotificationManager.IMPORTANCE_HIGH);
            notiChannel.setShowBadge(isShowBadge);
            notiChannel.enableVibration(isEnableVibe);
            notiChannel.setLightColor(Notification.FLAG_SHOW_LIGHTS);
            notiChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            notiChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            if (isEnableVibe)
            {
                notiChannel.setVibrationPattern(new long[]{1000, 1000});
            }

            if (isPlaySound)
            {
                Uri uri;
                try
                {
                    int notiSound = mPrefs.getInt(PREF_NOTI_SOUND);
                    if (notiSound > 0)
                    {
                        uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + notiSound);
                        CLog.d("notiSound " + notiSound + " uri " + uri.toString());
                    } else
                    {
                        throw new Exception("default ringtone is set");
                    }
                }
                catch (Exception e)
                {
                    uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    CLog.e(e.getMessage());
                }

                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();
                notiChannel.setSound(uri, audioAttributes);
                CLog.d("setChannelSound ring with initialize notichannel");
            } else
            {
                notiChannel.setSound(null, null);
                CLog.d("setChannelSound muted with initialize notichannel");
            }
            notificationManager.createNotificationChannel(notiChannel);
            return strNotiChannel;
        } else
        {
            CLog.d("notificationChannel is exist");
            return strNotiChannel;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void updateNotificationSummary (Context context, NotificationManager manager) {
        final StatusBarNotification[] activeNotifications = manager.getActiveNotifications();

        int numberOfNotifications = activeNotifications.length;
        // Since the notifications might include a summary notification remove it from the count if
        // it is present.
        for (StatusBarNotification notification : activeNotifications) {
            if (notification.getId() == NOTIFICATION_GROUP_SUMMARY_ID) {
                numberOfNotifications--;
            }
        }

        if (numberOfNotifications > 1)
        {
            NotificationCompat.Builder builder;

            String strNotiChannel = DataKeyUtil.getDBKey(context, DB_NOTI_CHANNEL_ID);
            if (StringUtil.isEmpty(strNotiChannel)) {
                strNotiChannel = "0";
            }
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
                strNotiChannel = createNotiChannel(context, manager, strNotiChannel);
                builder = new NotificationCompat.Builder(context, strNotiChannel);
            }
            else {
                builder = new NotificationCompat.Builder(context);
            }
            builder.setPriority(Notification.PRIORITY_MIN)
                    .setContentTitle(PMSUtil.getApplicationName(context))
                    .setContentText("새로운 메시지가 있습니다")
                    .setGroup(NOTIFICATION_GROUP)
                    .setGroupSummary(true);
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            for (StatusBarNotification notification : activeNotifications) {
                if (notification.getId() == NOTIFICATION_GROUP_SUMMARY_ID) {
                    numberOfNotifications--;
                }
                else
                {
                    inboxStyle.addLine(notification.getNotification().tickerText);
                }
            }
            builder.setStyle(inboxStyle);

            int iconId = 0;
            int largeIconId = 0;
            int ver = Build.VERSION.SDK_INT;
            if (ver >= Build.VERSION_CODES.LOLLIPOP) {
                iconId = PMSUtil.getIconId(context);
                largeIconId = PMSUtil.getLargeIconId(context);
            } else {
                iconId = PMSUtil.getLargeIconId(context);
            }

            // set small icon
            CLog.i("small icon :" + iconId);
            if (iconId > 0) {
                builder.setSmallIcon(iconId);
            }

            // set large icon
            CLog.i("large icon :" + largeIconId);
            if (largeIconId > 0) {
                builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), largeIconId));
            }


            manager.notify(NOTIFICATION_GROUP_SUMMARY_ID, builder.build());
        } else {
            // Remove the notification summary.
            manager.cancel(NOTIFICATION_GROUP_SUMMARY_ID);
        }
    }
}
